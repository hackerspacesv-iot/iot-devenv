# Hackerspace San Salvador IoT Development Framework

This repository contains a [Vagrant environment](https://www.vagrantup.com/)
to develop solutions within the [Hackerspace San Salvador IoT framework](https://gitlab.com/hackerspacesv-iot/doc).

This development environment is based on 
[Slackware-current](http://www.slackware.com/info/) and contains
an exhaustive collection of development tools for a long list
of programming languages.

In addition to the base-system this environment includes tools
to cross-compile to the following platforms:
- ARM Cortex
- Raspberry Pi
- Onion Omega 2
- BeagleBone Black

### Software requirements
To use this development environment you will require:
 - [Oracle Virtual Box 5.2](https://www.virtualbox.org/) 
with [Oracle VM VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads)
 - [Vagrant by HashiCorp](https://www.vagrantup.com/)

### How to use
1. Add the Vagrant box
    > vagrant box add hackerspacesv/iot-devenv
2. Clone this repository:
    > git clone https://gitlab.com/hackerspacesv-iot/iot-devenv
3. Change to the created repository
    > cd iot-devenv
4. Fire up vagrant
    > vagrant up
5. Connect to the vagrant host using ssh
    > vagrant ssh
6. We recommend to change to the shared "work" directory within
the VM so you can use your favorite text editor or IDE to work.
    > cd work
7. [Optional but highly recommended]: This image is based on Slackware-current.
Is highly recommended to check out the [security advisories](http://www.slackware.com/security/)
and update when is required. Run the following commands to update Slackware with 
the latests security updates.
    > sudo slackpkg update

    > sudo slackpkg upgrade-all

### Toolchains included with the environment
- [GNU Arm Embedded Toolchain 7-2018-q2-update](https://developer.arm.com/-/media/Files/downloads/gnu-rm/7-2018q2/gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2?revision=bc2c96c0-14b5-4bb4-9f18-bceb4050fee7?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,7-2018-q2-update)
- [Linux Embedded Development Environment](https://openwrt.org/)
- [Raspberry Pi Toolchain](https://github.com/raspberrypi/tools)
- [GCC Linaro 7.3.1-2018.05 for arm-linux-gnueabihf](https://releases.linaro.org/components/toolchain/binaries/7.3-2018.05/arm-linux-gnueabihf/)

### Additional software and features
- Open On-Chip-Debugger ([OpenOCD](http://openocd.org/))
- [Python 3](https://www.python.org/downloads/)
- Pre-Configured VirtualBox USB 2.0 filters for [Olimex ARM-USB-TINY JTAG Debugger](https://www.olimex.com/Products/ARM/JTAG/ARM-USB-TINY/) and the Hackerspace San Salvador [ATSAMR21 Breakout Board](https://hackerspace.teubi.co/wiki/HSSV_ATSAMR21_Breakout_English).

### Why Slackware?
There are many reasons but we can point a couple of them
that makes it very suitable for a development environment: 
* Is one of the oldest GNU/Linux distributions around widely
recognized by its stability and security.
* It's focused on simplicity: the whole system can be customized
with simple text-based configurations.
* Everything works as the developer of the included software meant
it to work. You don't need to worry about distribution-specific 
patchs or non-documented meta-configurations.
* Very low overhead that makes it ideal to run inside a VM.
* Follows the UNIX philosophy of making the tools small and 
modular. In summary: WYSIWYG.

## License
Copyright 2018 Mario Gómez  <mario.gomez@teubi.co>/Hackerspace San Salvador
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

